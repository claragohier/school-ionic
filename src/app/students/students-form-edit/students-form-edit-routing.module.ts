import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentsFormEditPage } from './students-form-edit.page';

const routes: Routes = [
  {
    path: '',
    component: StudentsFormEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentsFormEditPageRoutingModule {}
