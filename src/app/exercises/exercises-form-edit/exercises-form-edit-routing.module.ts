import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExercisesFormEditPage } from './exercises-form-edit.page';

const routes: Routes = [
  {
    path: '',
    component: ExercisesFormEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExercisesFormEditPageRoutingModule {}
