import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExercisesService } from '../../service/exercises.service';

@Component({
  selector: 'app-exercises-details',
  templateUrl: './exercises-details.page.html',
  styleUrls: ['./exercises-details.page.scss'],
})
export class ExercisesDetailsPage implements OnInit {

  constructor(private exercisesService: ExercisesService,
    private route: ActivatedRoute) { }

  id: string = this.route.snapshot.params.id;
  exercise: any;

  ngOnInit() {
    this.exercisesService.getExercise(this.id).subscribe(exercise => {
      this.exercise = exercise;
    });
  };

}
