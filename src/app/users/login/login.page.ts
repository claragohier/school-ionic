import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersService } from '../../service/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private usersService: UsersService) { }

  loginForm: FormGroup;

  ngOnInit(): void {
    this.initForm();
  }

  onSubmit() {
    this.usersService.login(this.loginForm.value).subscribe(login => {
      console.log(login);
    });
  };

  private initForm() {
    let username = "";
    let password = "";

    this.loginForm = new FormGroup({
      "username": new FormControl(username, Validators.required),
      "password": new FormControl(password, Validators.required)
    });
  };

}
