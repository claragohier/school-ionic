import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../../service/users.service';
import { Users } from '../../models/Users.model';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.page.html',
  styleUrls: ['./users-list.page.scss'],
})
export class UsersListPage implements OnInit {

  constructor(private router: Router,
    private usersService: UsersService) { }

  users: Users[];

  ngOnInit() {
    this.usersService.getUsers().subscribe(users => {
      this.users = users;
    });
  };

  // onUpdateStudent(id: string){
  //   this.router.navigate(['students-form-edit/' + id])
  // }

  // onGetOne(id: string){
  //   this.router.navigate(['students-details/' + id]);
  // }


}
