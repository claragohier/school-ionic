import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsersFormEditPage } from './users-form-edit.page';

describe('UsersFormEditPage', () => {
  let component: UsersFormEditPage;
  let fixture: ComponentFixture<UsersFormEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersFormEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UsersFormEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
