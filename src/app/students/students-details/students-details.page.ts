import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StudentsService } from '../../service/students.service';

@Component({
  selector: 'app-students-details',
  templateUrl: './students-details.page.html',
  styleUrls: ['./students-details.page.scss'],
})
export class StudentsDetailsPage implements OnInit {

  constructor(private studentsService: StudentsService,
    private route: ActivatedRoute) { }

  id = this.route.snapshot.params.id;
  student: any;

  ngOnInit() {
    this.studentsService.getStudent(this.id).subscribe(student => {
      this.student = student;
      console.log(student);
    });
  }
}
