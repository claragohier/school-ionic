import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StudentsService } from '../../service/students.service';
import { Students } from '../../models/Students.model';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.page.html',
  styleUrls: ['./students-list.page.scss'],
})
export class StudentsListPage implements OnInit {

  constructor(private router: Router,
    private studentsService: StudentsService) { }

  students: Students[];

  ngOnInit() {
    this.studentsService.getStudents().subscribe(students => {
      this.students = students;
    });
  };

  onDeleteStudent(id: string) {
    if (window.confirm('Etes-vous sûr.e de vouloir supprimer?')) {
      this.studentsService.deleteStudent(id).subscribe(student => {
        console.log(student);
      });
    };
  }

  onUpdateStudent(id: string){
    this.router.navigate(['students-form-edit/' + id])
  }

  onGetOne(id: string){
    this.router.navigate(['students-details/' + id]);
  }

}
