import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsersFormEditPageRoutingModule } from './users-form-edit-routing.module';

import { UsersFormEditPage } from './users-form-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UsersFormEditPageRoutingModule
  ],
  declarations: [UsersFormEditPage]
})
export class UsersFormEditPageModule {}
