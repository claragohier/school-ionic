import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExerciseslistPageRoutingModule } from './exerciseslist-routing.module';

import { ExerciseslistPage } from './exerciseslist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExerciseslistPageRoutingModule
  ],
  declarations: [ExerciseslistPage]
})
export class ExerciseslistPageModule {}
