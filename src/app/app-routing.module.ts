import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'students-list',
    loadChildren: () => import('./students/students-list/students-list.module').then( m => m.StudentsListPageModule)
  },
  {
    path: '',
    redirectTo: 'students-list',
    pathMatch: 'full'
  },
  {
    path: 'students-form',
    loadChildren: () => import('./students/students-form/students-form.module').then( m => m.StudentsFormPageModule)
  },
  {
    path: 'students-form-edit/:id',
    loadChildren: () => import('./students/students-form-edit/students-form-edit.module').then( m => m.StudentsFormEditPageModule)
  },
  {
    path: 'students-details/:id',
    loadChildren: () => import('./students/students-details/students-details.module').then( m => m.StudentsDetailsPageModule)
  },
  {
    path: 'exerciseslist',
    loadChildren: () => import('./exercises/exerciseslist/exerciseslist.module').then( m => m.ExerciseslistPageModule)
  },
  {
    path: 'exercises-form',
    loadChildren: () => import('./exercises/exercises-form/exercises-form.module').then( m => m.ExercisesFormPageModule)
  },
  {
    path: 'exercises-form-edit/:id',
    loadChildren: () => import('./exercises/exercises-form-edit/exercises-form-edit.module').then( m => m.ExercisesFormEditPageModule)
  },
  {
    path: 'exercises-details/:id',
    loadChildren: () => import('./exercises/exercises-details/exercises-details.module').then( m => m.ExercisesDetailsPageModule)
  },
  {
    path: 'users-list',
    loadChildren: () => import('./users/users-list/users-list.module').then( m => m.UsersListPageModule)
  },
  {
    path: 'users-form',
    loadChildren: () => import('./users/users-form/users-form.module').then( m => m.UsersFormPageModule)
  },
  {
    path: 'users-form-edit/:id',
    loadChildren: () => import('./users/users-form-edit/users-form-edit.module').then( m => m.UsersFormEditPageModule)
  },
  {
    path: 'users-details/:id',
    loadChildren: () => import('./users/users-details/users-details.module').then( m => m.UsersDetailsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./users/login/login.module').then( m => m.LoginPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
