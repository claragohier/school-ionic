import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExercisesFormEditPageRoutingModule } from './exercises-form-edit-routing.module';

import { ExercisesFormEditPage } from './exercises-form-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExercisesFormEditPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ExercisesFormEditPage]
})
export class ExercisesFormEditPageModule {}
