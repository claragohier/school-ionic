import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Students } from '../models/Students.model';
import { Response } from '../models/Response.model';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private http: HttpClient) { }
  url: string = 'http://localhost:2400/students/';
  
  getStudents(){
    return this.http.get<Students[]>(this.url);
  };

  getStudent(id: string){
    return this.http.get<Students>(this.url + id);
  };

  createStudent(student: Students){
    return this.http.post<Response>(this.url, student);
  };

  updateStudent(id: string, student: Students){
    return this.http.put<Response>(this.url + id, student);
  };

  deleteStudent(id: string){
    return this.http.delete<Response>(this.url + id);
  };

}
