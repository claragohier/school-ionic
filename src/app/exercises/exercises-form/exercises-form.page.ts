import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ExercisesService } from '../../service/exercises.service';

@Component({
  selector: 'app-exercises-form',
  templateUrl: './exercises-form.page.html',
  styleUrls: ['./exercises-form.page.scss'],
})
export class ExercisesFormPage implements OnInit {

  constructor(private exercisesService: ExercisesService) { }

  exerciseForm: FormGroup;

  ngOnInit() {
    this.initForm();
  }

  onSubmit(){
    if(this.exerciseForm.valid){
      this.exercisesService.createExercise(this.exerciseForm.value).subscribe(result => {
        console.log(result);
      });
    };
  };

  private initForm(){
    let statement = "";
    let question = "";
    let response = "";

    this.exerciseForm = new FormGroup({
      "statement": new FormControl(statement, Validators.required),
      "question": new FormControl(question, Validators.required),
      "response": new FormControl(response, Validators.required)
    })
  }

}
