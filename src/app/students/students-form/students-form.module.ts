import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StudentsFormPageRoutingModule } from './students-form-routing.module';

import { StudentsFormPage } from './students-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StudentsFormPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [StudentsFormPage]
})
export class StudentsFormPageModule {}
