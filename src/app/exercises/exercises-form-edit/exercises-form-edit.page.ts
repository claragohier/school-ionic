import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ExercisesService } from '../../service/exercises.service';

@Component({
  selector: 'app-exercises-form-edit',
  templateUrl: './exercises-form-edit.page.html',
  styleUrls: ['./exercises-form-edit.page.scss'],
})
export class ExercisesFormEditPage implements OnInit {

  constructor(private exercisesService: ExercisesService,
    private route: ActivatedRoute) { }

  exerciseForm: FormGroup;
  id: string = this.route.snapshot.params.id;

  ngOnInit() {
    this.initForm();
  }

  onSubmit(id: string){
    if(this.exerciseForm.valid){
      this.exercisesService.updateExercise(this.id,this.exerciseForm.value).subscribe(result => {
        console.log(result);
      });
    };
  };

  private initForm(){
    let statement = "";
    let question = "";
    let response = "";

    this.exerciseForm = new FormGroup({
      "statement": new FormControl(statement, Validators.required),
      "question": new FormControl(question, Validators.required),
      "response": new FormControl(response, Validators.required)
    });
  };


}
