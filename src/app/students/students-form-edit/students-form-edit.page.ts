import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { StudentsService } from '../../service/students.service';

@Component({
  selector: 'app-students-form-edit',
  templateUrl: './students-form-edit.page.html',
  styleUrls: ['./students-form-edit.page.scss'],
})
export class StudentsFormEditPage implements OnInit {

  constructor(private studentsService: StudentsService,
    private route: ActivatedRoute) { }

  studentForm: FormGroup;
  id: string = this.route.snapshot.params.id;

  ngOnInit() {
    this.initForm();
  };

  onSubmit() {
    if (this.studentForm.valid) {
      this.studentsService.updateStudent(this.id, this.studentForm.value).subscribe(result => {
        console.log(result);
      });
    };
  };

  private initForm() {
    let firstname = "";
    let lastname = "";
    let age = "";
    let city = "";

    this.studentForm = new FormGroup({
      "firstname": new FormControl(firstname, Validators.required),
      "lastname": new FormControl(lastname, Validators.required),
      "age": new FormControl(age, Validators.required),
      "city": new FormControl(city, Validators.required)
    })
  };

}
