import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExercisesFormPageRoutingModule } from './exercises-form-routing.module';

import { ExercisesFormPage } from './exercises-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExercisesFormPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ExercisesFormPage]
})
export class ExercisesFormPageModule {}
