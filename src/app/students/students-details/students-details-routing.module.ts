import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentsDetailsPage } from './students-details.page';

const routes: Routes = [
  {
    path: '',
    component: StudentsDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentsDetailsPageRoutingModule {}
