import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ExercisesService } from '../../service/exercises.service';
import { Exercises } from '../../models/Exercises.model';

@Component({
  selector: 'app-exerciseslist',
  templateUrl: './exerciseslist.page.html',
  styleUrls: ['./exerciseslist.page.scss'],
})
export class ExerciseslistPage implements OnInit {

  constructor(private router: Router,
    private exercisesService: ExercisesService) { }

  exercises: Exercises[];

  ngOnInit() {
    this.exercisesService.getExercises().subscribe(exercises => {
      this.exercises = exercises;
    });
  };

  onDeleteExercise(id: string){
    if (window.confirm('Etes-vous sûr.e de vouloir supprimer?')) {
      this.exercisesService.deleteExercise(id).subscribe(exercise => {
        console.log(exercise);
      });
    };
  };

  onUpdateExercise(id: string){
    this.router.navigate(['exercises-form-edit/' + id]);
  };
  
  onGetOne(id: string){
    this.router.navigate(['exercises-details/' + id]);
  };
}
