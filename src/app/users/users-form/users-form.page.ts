import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersService } from '../../service/users.service';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.page.html',
  styleUrls: ['./users-form.page.scss'],
})
export class UsersFormPage implements OnInit {

  constructor(private usersService: UsersService) { }

  userForm: FormGroup;

  ngOnInit() {
    this.initForm();
  };

  onSubmit() {
    if (this.userForm.valid) {
      this.usersService.createUsers(this.userForm.value).subscribe(result => {
        console.log(result);
      });
    };
  };

  private initForm() {
    let username = "";
    let password = "";

    this.userForm = new FormGroup({
      "username": new FormControl(username, Validators.required),
      "password": new FormControl(password, Validators.required)
    });
  };

}
