import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExercisesFormEditPage } from './exercises-form-edit.page';

describe('ExercisesFormEditPage', () => {
  let component: ExercisesFormEditPage;
  let fixture: ComponentFixture<ExercisesFormEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExercisesFormEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExercisesFormEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
