import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersFormEditPage } from './users-form-edit.page';

const routes: Routes = [
  {
    path: '',
    component: UsersFormEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersFormEditPageRoutingModule {}
