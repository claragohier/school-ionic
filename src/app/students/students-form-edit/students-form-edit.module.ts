import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StudentsFormEditPageRoutingModule } from './students-form-edit-routing.module';

import { StudentsFormEditPage } from './students-form-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StudentsFormEditPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [StudentsFormEditPage]
})
export class StudentsFormEditPageModule {}
