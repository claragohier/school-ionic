import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentsFormPage } from './students-form.page';

const routes: Routes = [
  {
    path: '',
    component: StudentsFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentsFormPageRoutingModule {}
