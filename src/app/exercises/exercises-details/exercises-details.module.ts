import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExercisesDetailsPageRoutingModule } from './exercises-details-routing.module';

import { ExercisesDetailsPage } from './exercises-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExercisesDetailsPageRoutingModule
  ],
  declarations: [ExercisesDetailsPage]
})
export class ExercisesDetailsPageModule {}
