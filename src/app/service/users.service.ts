import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Users } from '../models/Users.model';
import { Response } from '../models/Response.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }
  
  url: string = 'http://localhost:2400/users/';

  getUsers(){
    return this.http.get<Users[]>(this.url);
  };

  createUsers(user: Users){
    return this.http.post<Response>(this.url, user);
  };

  login(user: Users){
    return this.http.post<Response>(this.url + '/login', user);
  };

}
