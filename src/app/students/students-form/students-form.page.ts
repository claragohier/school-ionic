import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StudentsService } from '../../service/students.service';

@Component({
  selector: 'app-students-form',
  templateUrl: './students-form.page.html',
  styleUrls: ['./students-form.page.scss'],
})
export class StudentsFormPage implements OnInit {

  constructor(private studentsService: StudentsService) { }

  studentForm: FormGroup;

  ngOnInit() {
    this.initForm();
  };

  onSubmit() {
    if (this.studentForm.valid) {
      this.studentsService.createStudent(this.studentForm.value).subscribe(result => {
        console.log(result);
      });
    };
  };

  private initForm() {
    let firstname = "";
    let lastname = "";
    let age = "";
    let city = "";

    this.studentForm = new FormGroup({
      "firstname": new FormControl(firstname, Validators.required),
      "lastname": new FormControl(lastname, Validators.required),
      "age": new FormControl(age, Validators.required),
      "city": new FormControl(city, Validators.required)
    });
  };

}
