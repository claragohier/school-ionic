import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StudentsFormEditPage } from './students-form-edit.page';

describe('StudentsFormEditPage', () => {
  let component: StudentsFormEditPage;
  let fixture: ComponentFixture<StudentsFormEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsFormEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StudentsFormEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
